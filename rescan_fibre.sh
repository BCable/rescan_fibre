#!/bin/bash

# rescan_fibre.sh
# Author: Brad Cable
# Version: 1.0

systemctl stop multipathd

ls -1 /sys/class/fc_host | while read line; do
	echo 1 > "/sys/class/fc_host/$line/issue_lip"
done

ls -1 /sys/class/scsi_host | while read line; do
	echo "- - -" > "/sys/class/scsi_host/$line/scan"
done

ls -1d /sys/block/sd* | while read line; do
	echo 1 > "$line/device/rescan"
done

multipath -F
systemctl start multipathd
